from django.contrib.auth.models import User
from django.db import models

class WallMessage(models.Model):

    user = models.ForeignKey(User)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.user.email + ': ' + self.message[:50]  # Primeros 50 caracteres.