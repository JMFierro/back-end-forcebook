
__author__ = 'jose.fierro'


from django.conf.urls import patterns, url
from api import WallMessageList, WallMessageDetail, Signup

urlpatterns = patterns('',
    url(r'^api/messages/$', WallMessageList.as_view()),
    url(r'^api/messages/(?P<pk>[0-9]+)$', WallMessageDetail.as_view()),

    url(r'^api/signup$', Signup.as_view()),
                     )