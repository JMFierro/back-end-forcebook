from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from planets.api import PlanetList, PlanetDetail
from social_network.api import WallMessageList, WallMessageDetail, Signup


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'forcebook_src.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'social_network.views.app_login'),
    url(r'^logout', 'social_network.views.app_logout'),
    url(r'^admin/', include(admin.site.urls)),

    # Anadir url (conecta url con el controlador)
    url(r'^planets/','planets.views.planets_list'),

    url(r'^user/(?P<url_username>.+)', 'social_network.views.wall'),  # Acepta parametros, y con .+ acepta cualquier caracter

    # API Rest
    url(r'^api/planets/(?P<id>[0-9]+)', PlanetDetail.as_view()),
    url(r'^api/planets/', PlanetList.as_view()),
    # url(r'^api(images/', ImageAPI.as_view()),

    # url(r'^api/messages/$', WallMessageList.as_view()),
    # url(r'^api/messages/(?P<pk>[0-9]+)$', WallMessageDetail.as_view()),
    #
    # url(r'^api/signup$', Signup.as_view()),

    url(r'^api/1.0', include('social_network.api_urls')),

    # agrega
    url(r'^api/', include('fbusers.api_urls')),
)
