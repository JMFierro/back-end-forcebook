__author__ = 'jose.fierro'

from rest_framework.viewsets import ModelViewSet  # ReadOnlyModelViewSet
from serializers import UserSerializer
from django.contrib.auth.models import User

class UserViewSet(ModelViewSet):  # ReadOnlyModelViewSet):

    queryset = User.objects.filter(is_active=True) # solo usuarios activos
    serializer_class = UserSerializer