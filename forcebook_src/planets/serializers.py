__author__ = 'jose.fierro'

from rest_framework import serializers
from models import Planet


class PlanetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Planet
        fields = ('id','name')

class PlanetDetailSerializer(PlanetSerializer):

    class Meta(PlanetSerializer.Meta):
        fields = None # mostrar todos los campos

# class PlanetSerializer(serializers.Serializer):

    # id = serializers.Field()
    # name = serializers.CharField(max_length=75)
    # system = serializers.CharField(max_length=75)
    # sector = serializers.CharField(max_length=75)
    # galactic_longitude = serializers.FloatField()
    # galactic_latitude = serializers.FloatField()
    #
    # def restore_object(self, attrs, instance=None):
    #
    #     # Se crea el objeto
    #     if instance is None:
    #
    #         planet = Planet()
    #         planet.name = attrs.get('name','')
    #         planet.system = attrs.get('system','')
    #         planet.sector = attrs.get('sector','')
    #         planet.galactic_latitude = attrs.get('galactic_latitude',0.0)
    #         planet.galactic_longitude = attrs.get('galactic_longitude',0.0)
    #         return planet
    #
    #     else:
    #
    #         instance.name = attrs.get('name',instance.name)
    #         instance.system = attrs.get('system',instance.system)
    #         instance.sector = attrs.get('sector', instance.sector)
    #         instance.galactic_latitude = attrs.get('galactic_latitude',instance.galactic_latitude)
    #         instance.galactic_longitude = attrs.get('galactic_longitude', instance.galactic_longitude)
    #         return instance
