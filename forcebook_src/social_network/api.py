from django.contrib.auth.hashers import make_password

__author__ = 'jose.fierro'

from rest_framework import views
from serializers import WallMessageSerializer, WallMessageListSerializer, UserSignupSerializer
from models import  WallMessage
from rest_framework.response import Response
from rest_framework import  status
from rest_framework import generics
from rest_framework import permissions
from permissions import WallMessagePermissions
from django.contrib.auth.models import User

class WallMessageList(generics.ListCreateAPIView):

    queryset = WallMessage.objects.all()
    serializer_class = WallMessageListSerializer # para que no muestre el detalle en el listado generico
    permission_classes =  (permissions.IsAuthenticated, WallMessagePermissions,)

    # en este metodo se pueden toquetear los datos del usuario
    def get_queryset(self):
        logged_user = self.request.user   # cogemos el ususario autentificado
        # filtro por el usuario autentificado
        return self.queryset.filter(user=logged_user)   #igual a: WallMessage.objects.filter(user=logged_user)

    def pre_save(self, obj):
        obj.user = self.request.user


class WallMessageDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = WallMessage.objects.all()
    serializer_class = WallMessageSerializer
    permission_classes =  (permissions.IsAuthenticatedOrReadOnly, WallMessagePermissions,)


    def pre_save(self, obj):
        obj.user = self.request.user


    # # el usuario solo podra ver los mensages que son solo suyos
    # def get_queryset(self):
    #     logged_user = self.request.user   # cogemos el ususario autentificado
    #     # filtro por el usuario autentificado
    #     return self.queryset.filter(user=logged_user)

# class WallMessageList(views.APIView):
#
#     def get(self, request):
#         message_list = WallMessage.objects.all()
#         serializer = WallMessageSerializer(message_list, many=True)
#         return Response(serializer.data)
#
#     def post(self, request):
#         serializer = WallMessageSerializer(data=request.DATA)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         else:
#             return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)


class Signup(views.APIView):

    def post(self, request):

        serializer = UserSignupSerializer(data=request.DATA)

        if serializer.is_valid():
            new_user = serializer.object # recuperamos el usuario serializado
            new_django_user = User()
            new_django_user.first_name = new_user.first_name
            new_django_user.last_name = new_user.last_name
            new_django_user.username = new_user.username

            # genera clave de usuario con el mecanismo de django
            new_django_user.password = make_password(new_user.password)
            new_django_user.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)