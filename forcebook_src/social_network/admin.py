from django.contrib import admin
from models import WallMessage

class WallMessageAdmin(admin.ModelAdmin):
    pass

admin.site.register(WallMessage, WallMessageAdmin)