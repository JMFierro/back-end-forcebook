from django.contrib import admin
from models import Planet

class PlanetAdmin(admin.ModelAdmin):

    list_display = ('name', 'system', 'sector', 'coodinates')
    list_filter = ('system','sector')
    search_fields = ('name', 'system', 'sector')

    fieldsets = (
        ('Nombre del planeta', {
            'fields' : ('name',)
        }),
        ('Sistema y sector', {
           'fields' : (('system', 'sector'),),
           'classes' : ('wide',)
        })

    )

    def coodinates(self,obj):
        return str(obj.galactic_latitude) + ',' + str(obj.galactic_longitude)

    # Para ordenar por columna coodinates
    coodinates.admin_order_field = 'galactic_latitude'

admin.site.register(Planet, PlanetAdmin)

