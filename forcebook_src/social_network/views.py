# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseServerError
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from  models import WallMessage

def app_logout(request):
    logout(request)
    return redirect('/')


def app_login(request):

    # Autentificar usuario
    errors = []
    if request.POST and 'username' in request.POST and 'password' in request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)  # Autentifica al usuario en la sesion
        else:
            errors.append('Usuario o contraseña incorrecta')


    # Si el ussuario esta autentificardo
    if request.user.is_authenticated():

        # Si esta el parametro next' en la url reedirige al muro del usuario ??
        if 'next' in request.GET:
            return redirect(request.GET['next'])
        else:
            return redirect('/users' + request.user.username)

        return redirect('/user/' + request.user.username)

    # Si el usuario no esta autentificado muestra errores
    context = {
        'errors' : errors
    }
    return render(request, 'social_network/login.html', context)


@login_required(login_url='/')
def wall(request, url_username):  ## Muestra los mensages de un usuario
    try:
        user = User.objects.get(username=url_username)  # porque campo queremos buscar
    except User.DoesNotExist:
        raise Http404
    except:
        raise HttpResponseServerError

    errors = []

    if len(request.POST) > 0 and 'message_text' in request.POST:

        if not request.user.is_authenticated():
            errors.append('No estás autentificado')
        elif request.user != user:
            errors.append('No tienes autorización para escribir')
        else:
            new_message = WallMessage()
            new_message.user = user
            new_message.message = request.POST['message_text']
            new_message.save()

    # Recuperar los mensajes del usuario
    user_messages = user.wallmessage_set.all()

    #Crear un contexto para darselo a la vista
    context = {
        'logged_user' : request.user,
        'user' : user,
        'messages' : user_messages,
        'errors' : errors,
    }

    # devolver la plantilla renderizada con el contexto y todo
    return render(request, 'social_network/wall.html', context)