__author__ = 'jose.fierro'

from models import Planet
from serializers import PlanetSerializer, PlanetDetailSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import  APIView

class PlanetList(APIView):

    def get(self,request):
         if request.DATA:
            serializer = PlanetSerializer(data=request.DATA)
            serializer.save()

         else:
            all_planets = Planet.objects.all()
            serializer = PlanetSerializer(all_planets, many=True)

         return Response(serializer.data)


class PlanetDetail(APIView):

    def get(self,request, id):
        try:
            planet = Planet.objects.get(id=id)
        except Planet.DoesNotExist:
            return Response(status=404)
        serializer = PlanetDetailSerializer(planet)
        return Response(serializer.data)