__author__ = 'jose.fierro'

from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter
from api import UserViewSet

#Crea router y registra viewset
router = DefaultRouter()
router.register(r'user', UserViewSet)

# publica URL para importar proyecto
urlpatterns = patterns('',
     url(r'', include(router.urls))
)
