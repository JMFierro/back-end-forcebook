from django.db import models

class Planet(models.Model):


    # Tupla de tuplas
    SYSTEM = (
        ('BIN', 'Binary'),
        ('BIN', 'Bakunra'),
        ('KOR', 'Koros'),
        ('RAF', 'Rafa'),
    )

    SECTORS = (
        ('ARK', 'Arkanis'),
        ('COR', 'Cornuscant'),
        ('KID', 'Kidriff'),
        ('TOB', 'Tobaskin'),
    )

    name = models.CharField(max_length=256)
    system = models.CharField(max_length=256, choices=SYSTEM)
    sector = models.CharField(max_length=256, choices=SECTORS)
    galactic_longitude = models.FloatField(null=True) # optional
    galactic_latitude = models.FloatField(null=True)



    def __unicode__(self):
        return self.name + ' (' + self.system + ')'

