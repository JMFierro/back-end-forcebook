__author__ = 'jose.fierro'

from rest_framework import permissions

class WallMessagePermissions(permissions.BasePermission):

    # solo en detalle
    def has_object_permission(self, request, view, obj):
        if request.user == obj.user:
            return True
        else:
            return False

    # siempre se ejecuta
    def has_permission(self,request, view):
        return True

        # print "WallMessagePermissions.has_permission"
        # return True

        # if  request.method not in permissions.SAFE_METHODS: ## == 'POST':
        #
        #     if 'user' in request.DATA and str(request.user.id) == str(request.DATA['user']):  # CASH se fuerza a que los dos sean string
        #         return True
        #     else:
        #         return False  # Si no es el muro del usuario
        #
        # else:   # Si es SAFE_METHODS
        #     return True

