from django.template.defaultfilters import first

__author__ = 'jose.fierro'

from rest_framework import serializers
from models import WallMessage
from django.contrib.auth.models import User


class WallMessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = WallMessage
        fields = ('id', 'message', 'created_at',)
        # que se muestre y que no se pueda cambiar
        read_only_fields = ('user',)

    def validate_message(self, attrs, source):

        message = attrs[source]

        if 'joe' in message:
            raise serializers.ValidationError('El mensaje no puede ser joe')
        else:
            return attrs # todo ha ido bien

    # def validate(self, attrs):
    #
    #     if attrs['user'].username in attrs['message']:
    #         raise serializers.ValidationError('No se puede utilizar el nombre del usuario')
    #     else:
    #         return attrs


class WallMessageListSerializer(WallMessageSerializer):

    class Meta(WallMessageSerializer.Meta):
        fields = ('id', 'message', )


    # def validate_user(self, attrs, source):
    #     user_id = attrs['user']  # igual que attrs[source]
    #     try:
    #         user = User.objects.get(id=user_id)
    #     except User.DoesNoExist:
    #         raise serializers.ValidationError('El usuario no existe')
    #     return attrs # todo ha ido bien


# creacion de un objeto intermedio para hacer el registro de usuarios
class UserSignupModel(object):

    def __init__(self,*args, **kwargs):
        self.first_name = kwargs.get('first_name', None)  # kwars['first_name']
        self.last_name = kwargs.get('last_name', None)
        self.username = kwargs.get('username',None)
        self.email = kwargs.get('email',None)
        self.password = kwargs.get('password',None)
        self.password_confirm = kwargs.get('password_confirm',None)





class UserSignupSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    username = serializers.CharField()
    password = serializers.CharField()
    password_confirm = serializers.CharField()

    def restore_object(self, attrs, instance=None):

        if instance is None:
            obj = UserSignupModel()
            obj.first_name = attrs.get('first_name',None)
            obj.last_name = attrs.get('last_name',None)
            obj.username = attrs.get('username',None)
            obj.password = attrs.get('password',None)
            obj.password_confirm = attrs.get('password_confirm',None)
            return obj

        else:
            instance.first_name = attrs.get('first_name', instance.first_name)
            instance.last_name = attrs.get('last_name', instance.last_name)
            instance.username = attrs.get('username', instance.username)
            instance.password = attrs.get('password', instance.password)
            instance.password_confirm = attrs.get('password_confirm', instance.password_confirm)

            return instance



    def validate(self, attrs):

        if 'password' in attrs and 'password_confirm' in attrs:
            if attrs['password'] != attrs['password_confirm']:
                raise serializers.ValidationError('Las password no son iguales')

        # try:
        #     # compueba si existe el usuario en la base de datos
        #     existent_user = User.objects.get(username=attrs['username'])
        # except User.DoesNotExit: # sin el usuario no existe, se lanza una excepcion
        #     existent_user = None
        #
        # if existent_user is not None:
        #     raise serializers.ValidationError('Ya exite el usuario')
        #
        # return attrs # todo va bien

        # forma mas corta
        existent_user = User.objects.filter(username=attrs['username'])

        if len(existent_user) > 0:
            raise serializers.ValidationError('Ya exite el usuario')

        return attrs