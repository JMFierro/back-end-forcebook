from django.http import HttpResponse
from django.shortcuts import render
from planets.models import Planet


def planets_list(request):

    listado_planetas = Planet.objects.all()
    planets = Planet.objects.all()
    context = {'planets' : listado_planetas }

    return render(request, 'planets/list.html', context)

